CREATE EXTENSION dblink;
DO
$body$
BEGIN
  IF NOT EXISTS(SELECT 1 FROM pg_catalog.pg_user WHERE usename = 'pgbench_user') THEN
    CREATE ROLE pgbench_user WITH LOGIN CREATEDB PASSWORD 'pgbench_pass';
  END IF;
  IF NOT EXISTS(SELECT 1 FROM pg_catalog.pg_user WHERE usename = 'pglogical') THEN
    CREATE ROLE pglogical WITH LOGIN REPLICATION SUPERUSER PASSWORD 'pglogical';
  END IF;
END
$body$;

DO
$body$
BEGIN
  IF NOT EXISTS(SELECT 1 FROM pg_database WHERE datname = 'pgbench_db') THEN
    PERFORM dblink_exec('dbname=' || current_database(), 'CREATE DATABASE pgbench_db OWNER pgbench_user');
  END IF;
END
$body$;

SELECT dblink_exec('dbname=pgbench_db', 'CREATE EXTENSION IF NOT EXISTS pglogical');
SELECT dblink_exec('dbname=template1', 'CREATE EXTENSION IF NOT EXISTS pglogical');
SELECT dblink_exec('dbname=postgres', 'CREATE EXTENSION IF NOT EXISTS pglogical');

SELECT dblink_exec('dbname=pgbench_db', 'CREATE EXTENSION IF NOT EXISTS pgl_ddl_deploy');
SELECT dblink_exec('dbname=template1', 'CREATE EXTENSION IF NOT EXISTS pgl_ddl_deploy');
SELECT dblink_exec('dbname=postgres', 'CREATE EXTENSION IF NOT EXISTS pgl_ddl_deploy');