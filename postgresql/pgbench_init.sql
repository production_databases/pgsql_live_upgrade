BEGIN
DROP TABLE IF EXISTS pgbench_history;
CREATE TABLE IF NOT EXISTS pgbench_history( id BIGSERIAL PRIMARY KEY, tid INT, bid INT, aid INT, delta INT, mtime TIMESTAMP, filler CHAR(22) );

DROP TABLE IF EXISTS pgbench_tellers;
CREATE TABLE IF NOT EXISTS pgbench_tellers( tid INT NOT NULL PRIMARY KEY, bid INT, tbalance INT, filler CHAR(84) );

DROP TABLE IF EXISTS pgbench_accounts;
CREATE TABLE IF NOT EXISTS pgbench_accounts( aid INT NOT NULL PRIMARY KEY, bid INT, abalance INT, filler CHAR(84) );

DROP TABLE IF EXISTS pgbench_branches;
CREATE TABLE IF NOT EXISTS pgbench_branches( bid INT NOT NULL PRIMARY KEY, bbalance INT, filler CHAR(88) );

ALTER TABLE pgbench_tellers ADD FOREIGN key (bid) REFERENCES pgbench_branches;
ALTER TABLE pgbench_accounts ADD FOREIGN key (bid) REFERENCES pgbench_branches;
ALTER TABLE pgbench_history ADD FOREIGN key (bid) REFERENCES pgbench_branches;
ALTER TABLE pgbench_history ADD FOREIGN key (tid) REFERENCES pgbench_tellers;
ALTER TABLE pgbench_history ADD FOREIGN key (aid) REFERENCES pgbench_accounts;


INSERT INTO pgbench_branches(bid,bbalance) VALUES(0,0);
INSERT INTO pgbench_branches(bid,bbalance) VALUES(1,0);
INSERT INTO pgbench_branches(bid,bbalance) VALUES(2,0);
INSERT INTO pgbench_branches(bid,bbalance) VALUES(3,0);
INSERT INTO pgbench_branches(bid,bbalance) VALUES(4,0);
INSERT INTO pgbench_branches(bid,bbalance) VALUES(5,0);
INSERT INTO pgbench_branches(bid,bbalance) VALUES(6,0);
INSERT INTO pgbench_branches(bid,bbalance) VALUES(7,0);
INSERT INTO pgbench_branches(bid,bbalance) VALUES(8,0);
INSERT INTO pgbench_branches(bid,bbalance) VALUES(9,0);
INSERT INTO pgbench_branches(bid,bbalance) VALUES(10,0);
INSERT INTO pgbench_branches(bid,bbalance) VALUES(11,0);
INSERT INTO pgbench_branches(bid,bbalance) VALUES(12,0);
INSERT INTO pgbench_branches(bid,bbalance) VALUES(13,0);
INSERT INTO pgbench_branches(bid,bbalance) VALUES(14,0);

COMMIT;