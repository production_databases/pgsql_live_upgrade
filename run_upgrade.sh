#!/usr/bin/env bash
set -x

set +e
vagrant destroy --force
set -e

# Basic Setup
export PGSQL_VERSION=9.5
export PGSQL_MASTER=pgsql1

vagrant up pgsql1 --provision-with=pgsql-server-setup
vagrant up pgsql2 --provision-with=pgsql-server-setup

vagrant up pgsql-proxy --provision-with=pgbouncer-setup
vagrant up pgsql-proxy --provision-with=pgrepup-setup

# Live upgrade PostgreSQL 9.5 to 10
export PGSQLBENCH_SECONDS=1800
export PGSQL_VERSION=10
export PGSQL_OLD_VERSION=9.5
vagrant up pgsql-proxy --provision-with=pgsqlbech
vagrant up pgsql2 --provision-with=gracefull-stop-slave
vagrant up pgsql2 --provision-with=pgsql-server-upgrade
export PGSQL_MASTER=pgsql1
export PGSQL_SLAVE=pgsql2
vagrant up pgsql-proxy --provision-with=switchover-slave-as-master

vagrant up pgsql1 --provision-with=pgsql-server-upgrade

export PGSQL_MASTER=pgsql2
export PGSQL_SLAVE=pgsql1
vagrant up pgsql-proxy --provision-with=switchover-slave-as-master
vagrant up pgsql-proxy --provision-with=data-validate

vagrant ssh pgsql1 -- "sudo -H -u postgres psql -c 'SELECT version();' postgres"
vagrant ssh pgsql2 -- "sudo -H -u postgres psql -c 'SELECT version();' postgres"



