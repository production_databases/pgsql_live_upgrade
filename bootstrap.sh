#!/usr/bin/env bash
set -xeuo pipefail
export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get install -y wget ca-certificates apt-transport-https
# sudo apt-get install -y aptitude mc htop jq iotop sysstat nmap
# source /vagrant/orchestrator-repo.sh
source /vagrant/pgsql-repo.sh
source /vagrant/pglogical-repo.sh
apt-get update