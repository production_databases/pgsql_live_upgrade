# -*- mode: ruby -*-
# vi: set ft=ruby :
# PGSQL live upgrade Vagrant stand

Vagrant.configure("2") do |config|
    config.vm.box = "ubuntu/xenial64"

    config.vm.provider :virtualbox do |vb|
        vb.customize ["modifyvm", :id, "--memory", 512]
        vb.customize ["modifyvm", :id, "--cpus", 2]
    end

    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
    config.hostmanager.ignore_private_ip = false
    config.hostmanager.include_offline = true

    config.vm.provision "pgsql-server-setup", type: "shell", args: ["#{ENV['PGSQL_VERSION']}"], inline: <<-SHELL
        set -xeuo pipefail
        export PGSQL_VERSION=$1
        source /vagrant/bootstrap.sh
        apt-get install -y postgresql-${PGSQL_VERSION} postgresql-contrib-${PGSQL_VERSION} postgresql-client-${PGSQL_VERSION}
        apt-get install -y postgresql-${PGSQL_VERSION}-pglogical
        apt-get install -y postgresql-${PGSQL_VERSION}-pgl-ddl-deploy
        cp -fv /vagrant/postgresql/conf.d/*.conf /etc/postgresql/${PGSQL_VERSION}/main/conf.d/
        cp -fv /vagrant/postgresql/pg_hba.conf /etc/postgresql/${PGSQL_VERSION}/main/
        systemctl enable postgresql
        systemctl restart postgresql
        sudo -H -u postgres psql -e < /vagrant/postgresql/init_db.sql
        sudo -H -u postgres psql -e pgbench_db < /vagrant/postgresql/pglogical.sql
        sudo -H -u postgres psql -e postgres  < /vagrant/postgresql/pglogical.sql
        sudo -H -u postgres psql -e template1 < /vagrant/postgresql/pglogical.sql
    SHELL

    config.vm.provision "pgsql-server-upgrade", type: "shell", args: ["#{ENV['PGSQL_VERSION']}","#{ENV['PGSQL_OLD_VERSION']}"], inline: <<-SHELL
        set -xeuo pipefail
        export PGSQL_VERSION=$1
        export PGSQL_OLD_VERSION=$2
        apt-get install -y postgresql-${PGSQL_VERSION} postgresql-contrib-${PGSQL_VERSION} postgresql-client-${PGSQL_VERSION}
        apt-get install -y postgresql-${PGSQL_VERSION}-pglogical
        apt-get install -y postgresql-${PGSQL_VERSION}-pgl-ddl-deploy
        cp -fv /vagrant/postgresql/conf.d/*.conf /etc/postgresql/${PGSQL_VERSION}/main/conf.d/
        cp -fv /vagrant/postgresql/pg_hba.conf /etc/postgresql/${PGSQL_VERSION}/main/
        # use --link options for speed
        pg_dropcluster --stop ${PGSQL_VERSION} main
        pg_upgradecluster -v ${PGSQL_VERSION} ${PGSQL_OLD_VERSION} main
        systemctl enable postgresql
        systemctl restart postgresql
    SHELL


    config.vm.provision "pgbouncer-setup", type: "shell", args: ["#{ENV['PGSQL_VERSION']}"], inline: <<-SHELL
        set -xeuo pipefail
        export PGSQL_VERSION=$1
        source /vagrant/bootstrap.sh
        apt-get install -y pgbouncer postgresql-client-${PGSQL_VERSION} postgresql-client-common
        cp -fv /vagrant/pgbouncer/* /etc/pgbouncer/
        systemctl enable pgbouncer
        systemctl restart pgbouncer
        systemctl status pgbouncer
        echo "Waiting pgbouncer to launch on 6432..."
        while ! nc -z 127.0.0.1 6432; do
        sleep 0.1
        done
        # {host}:{port}:{db}:{user}:{password}
        echo "localhost:6432:*:pgbouncer_stats:pgbouncer_stats" > ~/.pgpass
        chmod 0600 ~/.pgpass
        psql -p 6432 -U pgbouncer_stats -c 'SHOW POOLS' pgbouncer
    SHELL

    config.vm.provision "pgrepup-setup", type: "shell", inline: <<-SHELL
        set -xeuo pipefail
        apt-get install -y python-pip
        pip2 install -U psycopg2-binary
        pip2 install -U https://github.com/Slach/pgrepup/archive/master.zip
        mkdir -p /etc/pgrepup/
        cp -fv /vagrant/pgrepup/pgrepup*.conf /etc/pgrepup/
        # run this over vagrant ssh when you need rerenerate pgrepup config
        # pgrepup -c /etc/pgrepup/pgrepup_pgsql1.conf config
        pgrepup -c /etc/pgrepup/pgrepup_pgsql1.conf check
        pgrepup -c /etc/pgrepup/pgrepup_pgsql1.conf stop
        pgrepup -c /etc/pgrepup/pgrepup_pgsql1.conf setup
        pgrepup -c /etc/pgrepup/pgrepup_pgsql1.conf start
        PGREPL_ERRORS=0
        if [[ $(pgrepup -c /etc/pgrepup/pgrepup_pgsql1.conf status | grep -i -E "(\.\.down|Skipped)"| wc -l) != ${PGREPL_ERRORS} ]]; then
           pgrepup -c /etc/pgrepup/pgrepup_pgsql1.conf status
           exit 1
        fi
        EXPECTED_REPLICATING=3
        if [[ $(pgrepup -c /etc/pgrepup/pgrepup_pgsql1.conf status | grep -i -E "replicating"| wc -l) != ${EXPECTED_REPLICATING} ]]; then
           pgrepup -c /etc/pgrepup/pgrepup_pgsql1.conf status
           exit 1
        fi

    SHELL

    config.vm.provision "pgsqlbech", type: "shell", args: ["#{ENV['PGSQLBENCH_SECONDS']}","#{ENV['PGSQL_VERSION']}"], inline: <<-SHELL
        set -xeuo pipefail
        export PGSQLBENCH_SECONDS=$1
        export PGSQL_VERSION=$2
        apt-get install --no-install-recommends -y postgresql-client
        # apt-get install -y postgresql-contrib-${PGSQL_VERSION}
        # echo "localhost:6432:pgbench_db:pgbench_user:pgbench_pass" > ~/.pgpass
        # chmod 0600 ~/.pgpass
        # pgbench -h localhost -p 6432 -U pgbench_user -i -s 15 pgbench_db
        # screen -d -m bash -c "pgbench -h localhost -p 6432 -U pgbench_user -P 1 -c 4 -j 2 -T ${PGSQLBENCH_SECONDS} pgbench_db &>>/var/log/pgbench-processing.log"

        curl -s https://packagecloud.io/install/repositories/akopytov/sysbench/script.deb.sh | sudo bash
        apt-get install -y sysbench

        sysbench --report-interval=1 --test=/usr/share/sysbench/tests/include/oltp_legacy/oltp.lua --db-driver=pgsql --pgsql-user=pgbench_user --pgsql-password=pgbench_pass --pgsql-host=127.0.0.1 --pgsql-port=6432 --pgsql-db=pgbench_db --oltp-read-only=off prepare
        echo $(date +'%Y-%m-%d %H:%M:%S') "Statring sysbench..." &> /var/log/sysbench-processing.log
        screen -d -m bash -c "sysbench --report-interval=1 --threads=1 --time=${PGSQLBENCH_SECONDS} --test=/usr/share/sysbench/tests/include/oltp_legacy/oltp.lua --db-driver=pgsql --pgsql-user=pgbench_user --pgsql-password=pgbench_pass --pgsql-host=127.0.0.1 --pgsql-port=6432 --pgsql-db=pgbench_db --oltp-read-only=off run &>>/var/log/sysbench-processing.log"

        echo "localhost:6432:*:pgbouncer_stats:pgbouncer_stats" > ~/.pgpass
        chmod 0600 ~/.pgpass
        psql -p 6432 -U pgbouncer_stats -c 'SHOW POOLS' pgbouncer
        psql -p 6432 -U pgbouncer_stats -c 'SHOW SERVERS' pgbouncer
        psql -p 6432 -U pgbouncer_stats -c 'SHOW CLIENTS' pgbouncer
    SHELL

    config.vm.provision "gracefull-stop-slave", type: "shell", inline: <<-SHELL
        set -xeuo pipefail
        service postgresql stop
    SHELL

    config.vm.provision "switchover-slave-as-master", type: "shell", args: ["#{ENV['PGSQL_MASTER']}","#{ENV['PGSQL_SLAVE']}"], inline: <<-SHELL
        set -xeuo pipefail
        export PGSQL_MASTER=$1
        export PGSQL_SLAVE=$2
        echo "localhost:6432:pgbouncer:pgbouncer_admin:pgbouncer_admin" > ~/.pgpass
        chown root:root ~/.pgpass
        chmod 0600 ~/.pgpass

        pgrepup -c /etc/pgrepup/pgrepup_${PGSQL_MASTER}.conf setup
        pgrepup -c /etc/pgrepup/pgrepup_${PGSQL_MASTER}.conf start
        EXPECTED_REPLICATING=3
        if [[ $(pgrepup -c /etc/pgrepup/pgrepup_${PGSQL_MASTER}.conf status | grep -i -E "replicating"| wc -l) != ${EXPECTED_REPLICATING} ]]; then
           pgrepup -c /etc/pgrepup/pgrepup_${PGSQL_MASTER}.conf status
           exit 1
        fi

        psql -p 6432 -U pgbouncer_admin -c 'PAUSE' pgbouncer
        pgrepup -c /etc/pgrepup/pgrepup_${PGSQL_MASTER}.conf stop
        pgrepup -c /etc/pgrepup/pgrepup_${PGSQL_MASTER}.conf uninstall
        pgrepup -c /etc/pgrepup/pgrepup_${PGSQL_SLAVE}.conf check
        pgrepup -c /etc/pgrepup/pgrepup_${PGSQL_SLAVE}.conf setup
        pgrepup -c /etc/pgrepup/pgrepup_${PGSQL_SLAVE}.conf start
        PGREPL_ERRORS=0
        if [[ $(pgrepup -c /etc/pgrepup/pgrepup_${PGSQL_SLAVE}.conf status | grep -i -E "(\.\.down|Skipped)"| wc -l) != ${PGREPL_ERRORS} ]]; then
           pgrepup -c /etc/pgrepup/pgrepup_${PGSQL_SLAVE}.conf status
           exit 1
        fi
        EXPECTED_REPLICATING=3
        if [[ $(pgrepup -c /etc/pgrepup/pgrepup_${PGSQL_SLAVE}.conf status | grep -i -E "replicating"| wc -l) != ${EXPECTED_REPLICATING} ]]; then
           pgrepup -c /etc/pgrepup/pgrepup_${PGSQL_SLAVE}.conf status
           exit 1
        fi

        SED_REPLACE="s/${PGSQL_MASTER}/${PGSQL_SLAVE}/g"
        sed -i -e "${SED_REPLACE}" /etc/pgbouncer/pgbouncer.ini
        psql -p 6432 -U pgbouncer_admin -c 'RELOAD' pgbouncer
        psql -p 6432 -U pgbouncer_admin -c 'RESUME' pgbouncer
        psql -p 6432 -U pgbouncer_admin -c 'SHOW SERVERS' pgbouncer

    SHELL

    config.vm.provision "data-validate", type: "shell", args: ["#{ENV['PGSQLBENCH_SECONDS']}"], inline: <<-SHELL
        set -xeuo pipefail
        export PGSQLBENCH_SECONDS=$1
        IS_DONE=$(ps auxf | grep screen | wc -l)
        TRIES=0
        DONE=1
        while [[ ${IS_DONE} != ${DONE} && $TRIES -ne ${PGSQLBENCH_SECONDS} ]]
        do
          IS_DONE=$(ps auxf | grep screen | wc -l)
          TRIES=$(($TRIES+1))
          if [[ ${IS_DONE} != ${DONE} ]]; then
            sleep 1
          fi
        done

        if [[ ${IS_DONE} != ${DONE} ]]; then
           exit 1
        fi
        # cat /var/log/pgbench-processing.log
        cat /var/log/sysbench-processing.log
    SHELL


    config.vm.define 'pgsql1' do |machine|
        machine.vm.network "private_network", ip: "10.77.7.2"
        machine.vm.host_name = "pgsql1"
        machine.hostmanager.aliases = ["pgsql1.vagrant.k8s-russian.video"]
        machine.vm.provider :virtualbox do |vb|
            vb.customize ["modifyvm", :id, "--memory", 1024]
        end
    end

    config.vm.define 'pgsql2' do |machine|
        machine.vm.network "private_network", ip: "10.77.7.3"
        machine.vm.host_name = "pgsql2"
        machine.hostmanager.aliases = ["pgsql2.vagrant.k8s-russian.video"]
        machine.vm.provider :virtualbox do |vb|
            vb.customize ["modifyvm", :id, "--memory", 1024]
        end
    end

    config.vm.define 'pgsql-proxy' do |machine|
        machine.vm.network "private_network", ip: "10.77.7.4"
        machine.vm.host_name = "pgsql-proxy"
        machine.hostmanager.aliases = ["pgsql-proxy.vagrant.k8s-russian.video"]
        machine.vm.provider :virtualbox do |vb|
            vb.customize ["modifyvm", :id, "--memory", 512]
        end
    end

end